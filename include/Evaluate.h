//
// Created by mang on 02.02.2020.
//

#ifndef _CALCULATOR_H
#define _CALCULATOR_H

#include <string>

class Evaluate {
public:
	enum class ErrorCodes {
		VALID,
		COMMENT,
		EMPTY_EXPRESSION,
		INVALID_EXPRESSION,
		INVALID_SYNTAX
	};

	/**
	 * @brief
	 * @return
	 */
	static int evaluate(char* term);
	/**
	 * @brief checks if a given character is operand or value
	 * @param c
	 * @return
	 */
	bool isOperand(char c, int pos);

	/**
	 * @brief utility function to find value of and operand
	 * @param c
	 * @return
	 */
	int value(char c) { return (c - '0'); }

	static Evaluate::ErrorCodes checkExpression(std::string term);


private:
	std::string m_term;
};


#endif //_CALCULATOR_H