//
// Created by mang on 02.02.2020.
//

#include <Evaluate.h>
#include <string>


Evaluate::ErrorCodes Evaluate::checkExpression(std::string term) {
	if (term[0] == '#') return Evaluate::ErrorCodes::COMMENT ;

	if (term.empty()) return Evaluate::ErrorCodes::EMPTY_EXPRESSION;

	return Evaluate::ErrorCodes::VALID;
};

//bool Evaluate::isOperand(std::string c, int pos) {
//	bool isOpperand = (c[pos] >= '0' && c[pos] <= '9');
//	return isOpperand;
//}
//
//int Evaluate::value(char c) {
//	int value = c - '0';
//	return value;
//}

//int Evaluate::evaluate(char* term) {
//// The first character must be an operand, find its value
//	int result = value(term[0]);
//
//	// Traverse the remaining characters in pairs
//	for (int i = 1; term[i]; i += 2) {
//		// The next character must be an operator, and
//		// next to next an operand
//		char opr = term[i], opd = term[i + 1];
//
//		// If next to next character is not an operand
//		if (!isOperand(opd)) { return -1; }
//
//		// Update result according to the operator
//		if (opr == '+') { result += value(opd); }
//		else {
//			if (opr == '-') { result -= value(opd); }
//			else {
//				if (opr == '*') { result *= value(opd); }
//				else {
//					if (opr == '/') {
//						result /= value(opd);
//
//						// If not a valid operator
//					} else { return -1; }
//				}
//			}
//		}
//	}
//	return result;
//}
